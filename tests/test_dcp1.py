#!/usr/bin/env python3

# test_dcp_1.py


"""
	Given a list of numbers and a number k, return whether any two numbers from the 
	list add up to k.

	For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.

	Bonus: Can you do this in one pass?
"""

from dcp1 import dcp1 as dcp
import pytest	

def test_numbers_1():
	assert dcp.numbers([10, 15, 3, 7], 17) == True

def test_numbers_2():
	assert dcp.numbers([10, 15, 3, 7], 16) == False

def test_numbers_3():
	assert dcp.numbers([8, 5, 9, 14], 17) == True

def test_numbers_4():
	assert dcp.numbers([8, 5, 9, 14], 16) == False



if __name__ == '__main__':
	pass
