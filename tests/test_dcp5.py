#!/usr/bin/env python3

# File: test_dcp5.py


"""Daily Coding Problem #5

cons(a, b) constructs a pair, and car(pair) and cdr(pair) returns 
the first and last element of that pair. For example, car(cons(3, 4)) 
returns 3, and cdr(cons(3, 4)) returns 4.

Given this implementation of cons:
	
	def cons(a, b):
		def pair(f):
			return f(a, b)
		return pair

Implement car and cdr.
"""

from dcp5 import dcp5 as dcp

# def test_cons():
# 	assert dcp.cons(3, 4) ==  # ?????

def test_car():
	assert dcp.car(dcp.cons(3, 4)) == 3

def test_cdr():
	assert dcp.cdr(dcp.cons(3, 4)) == 4
