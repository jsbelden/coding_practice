#!/usr/bin/env python3

# File: test_dcp7.py

"""Daily Coding Problem #7

Given the mapping a = 1, b = 2, ... z = 26, and an encoded message, 
count the number of ways it can be decoded.

For example, the message '111' would give 3, since it could be decoded 
as 'aaa', 'ka', and 'ak'.

You can assume that the messages are decodable. For example, '001' is not allowed.
"""

from dcp7 import dcp7 as dcp
import pytest

class TestDCP7(object):

	@classmethod
	def setup(self):
		self.obj = dcp.DCP7()
		# decoderRing = {"a":1, "b":2, "c":3, "d":4, "e":5, "f":6, "g":7, "h":8, "i":9, "j":10, "k":11, "l":12, "m":13, "n":14, "o":15, "p":16, "q":17, "r":18, "s":19, "t":20, "u":21, "v":22, "w":23, "x":24, "y":25, "z":26}
		self.expecteddecoderRing = {"1":"a", "2":"b", "3":"c", "4":"d", "5":"e", "6":"f", "7":"g", "8":"h", "9":"i", "10":"j", "11":"k", "12":"l", "13":"m", "14":"n", "15":"o", "16":"p", "17":"q", "18":"r", "19":"s", "20":"t", "21":"u", "22":"v", "23":"w", "24":"x", "25":"y", "26":"z"}
		self.testMsg = "111"
		self.expectedNumberOfTranslations = 3

		assert isinstance(self.obj, type(dcp.DCP7()))
		assert isinstance(self.obj.decoderRing, dict)
		assert self.obj.decoderRing == self.expecteddecoderRing
		# assert isinstance(self.obj.decode(self.testMsg), int)
		assert isinstance(self.obj._getSubstrings(self.testMsg), list)


	def test_1(self):		
		assert self.obj.decode(self.testMsg) == 3

	def test_2(self):
		assert self.obj.decode("123") == 6

	def test_InvalidInputFails(self):
		assert self.obj.decode("") ==
		assert self.obj.decode("000") == "Invalid input!"
		assert self.obj.decode("001") == "Invalid input!"
		assert self.obj.decode("010") == "Invalid input!"
		assert self.obj.decode("011") == "Invalid input!"
		assert self.obj.decode("100") == "Invalid input!"
		assert self.obj.decode("101") == "Invalid input!"
		assert self.obj.decode("110") == "Invalid input!"
		assert self.obj.decode("111") != "Invalid input!"
