#!/usr/bin/env python3

# File: test_dcp4.py

"""Daily Coding Problem 4

Given an array of integers, find the first missing positive integer in 
linear time and constant space. In other words, find the lowest positive 
integer that does not exist in the array. The array can contain duplicates 
and negative numbers as well.

For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.
You can modify the input array in-place.

"""

from dcp4 import dcp4 as dcp

def test_findMissing_1():
	assert dcp.findMissing([3, 4, -1, 1]) == 2

def test_findMissing_2():
	assert dcp.findMissing([1, 2, 0]) == 3

if __name__ == '__main__':
	main()
