#!/usr/bin/env python3

# File: dcp7.py

"""Daily Coding Problem #7

Given the mapping a = 1, b = 2, ... z = 26, and an encoded message, 
count the number of ways it can be decoded.

For example, the message '111' would give 3, since it could be decoded 
as 'aaa', 'ka', and 'ak'.

You can assume that the messages are decodable. For example, '001' is not allowed.
"""

import itertools

class DCP7(object):
	def __init__(self):
		# self.decoderRing = {"a":1, "b":2, "c":3, "d":4, "e":5, "f":6, "g":7, "h":8, "i":9, "j":10, "k":11, "l":12, "m":13, "n":14, "o":15, "p":16, "q":17, "r":18, "s":19, "t":20, "u":21, "v":22, "w":23, "x":24, "y":25, "z":26}
		self.decoderRing = {"1":"a", "2":"b", "3":"c", "4":"d", "5":"e", "6":"f", "7":"g", "8":"h", "9":"i", "10":"j", "11":"k", "12":"l", "13":"m", "14":"n", "15":"o", "16":"p", "17":"q", "18":"r", "19":"s", "20":"t", "21":"u", "22":"v", "23":"w", "24":"x", "25":"y", "26":"z"}
		self.decodedMsg = ""

	def decode(self, incomingMsg:str) -> int:
		if "0" in incomingMsg or inncomingMsg == "":
			return "Invalid input!"
		else:
			self.msg	= incomingMsg
			numberOfTranslations = 0
			substrings = self._getSubstrings(incomingMsg)

			print(substrings)

			for len(incomingMsg)
		

		return numberOfTranslations

	def _getSubstrings(self, string:str) -> list:
		length = len(string)
		substrings = [string[i:j+1] for i in range(length) for j in range(i, length)]

		for substr in substrings:
			if int(substr) < 0 or int(substr) > 26:
				substrings.remove(substr)

		return sorted(substrings, key=int)


	# def _getSubstrings(self, string:str) -> list:
	# 	substrings = list(string)
	# 	# print(substrings)

	# 	for i in itertools.permutations(string, 2):
	# 		temp = ""
	# 		for j in i:
	# 			temp += j
	# 		substrings.append(temp)

	# 	# print(substrings)

	# 	i = 0
	# 	for substr in substrings:
	# 		print(substr)
	# 		if int(substr) < 1 or int(substr) > 26:
	# 			print(substr)
	# 			# substrings.remove(substr)
	# 			substrings.remove(substrings[i])

	# 		i += 1

	# 	return sorted(list(set(substrings)), key=int)
