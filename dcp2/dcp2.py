#!/usr/bin/env python3

# File: dcp2.py


"""
Given an array of integers, return a new array such that each element at 
index i of the new array is the product of all the numbers in the original 
array except the one at i.

For example, if our input was [1, 2, 3, 4, 5], the expected output would 
be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output 
would be [2, 3, 6].

Follow-up: what if you can't use division?
"""

def 	function(numList):
	results = []
	i = 0
	while i < len(numList):
		subList = numList[:i] + numList[i+1:]
		product = 1
		for num in subList:
			product *= num
		results.append(product)
		i += 1
	return results


if __name__ == '__main__':
	pass
