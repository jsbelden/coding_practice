#!/usr/bin/env python3

# File: dcp4.py

"""Daily Coding Problem 4

Given an array of integers, find the first missing positive integer in 
linear time and constant space. In other words, find the lowest positive 
integer that does not exist in the array. The array can contain duplicates 
and negative numbers as well.

For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.
You can modify the input array in-place.

"""

def findMissing(numList):
	numList.sort()
	missingNumber = 0
	
	i = 0
	while i < len(numList):
		if numList[i] <= 0:
			pass
		else:
			if i + 1 < len(numList):
				if abs(numList[i] - numList[i+1]) > 1:
					missingNumber = numList[i] + 1
					break
			else:
				missingNumber = numList[i] + 1
		i += 1

	return missingNumber


if __name__ == '__main__':
	pass
