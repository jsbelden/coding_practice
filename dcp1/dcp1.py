#!/usr/bin/env python3

# file: dcp_1.py

"""
	Given a list of numbers and a number k, return whether any two numbers from the 
	list add up to k.

	For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.

	Bonus: Can you do this in one pass?
"""

def numbers(numList, k):
	result = False

	index = len(numList) - 1
	for num in numList:
		while index >= 1:
			if num + numList[index] == k:
				result = True

			index -= 1

	return result


if __name__ == '__main__':
	pass
